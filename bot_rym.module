<?php

/**
 * @file
 * RYM Bot stuff.
 */

/**
 * Implements hook_help().
 */
function bot_rym_help($path, $arg) {
  switch ($path) {
    case 'irc:features':
      return array(t('Rym'));
    case 'irc:features#rym':
      return t('Available commands: show tickets; show clients; ticket for [clientID]: [comment]; To punch in, pi [ticketID]: [comment]; To punch out, po [ticketID]: [comment]');
  }
}

function bot_rym_comment_presave($comment) {
  // TRIED to use EFQ, failed miserably
  // copied the following from core comment.module
  $query = db_select('comment', 'c');
  $query->innerJoin('node', 'n', 'n.nid = c.nid');
  $query->addTag('node_access');
  $previous_cid = $query
    ->fields('c', array('cid'))
    ->condition('c.nid', $comment->nid)
    ->condition('c.status', COMMENT_PUBLISHED)
    ->condition('n.status', NODE_PUBLISHED)
    ->orderBy('c.created', 'DESC')
    // Additionally order by cid to ensure that comments with the same timestamp
    // are returned in the exact order posted.
    ->orderBy('c.cid', 'DESC')
    ->extend('PagerDefault')
    ->limit(1)
    ->execute()
    ->fetchColumn();

  // first post?
  if ($previous_cid) {
    $previous_comment = comment_load($previous_cid);
  } else {
    $previous_comment = new stdclass;
    $previous_comment->field_state['und'][0]['value'] = 'open/discuss';
  }

  // set values to those of previous comment
  // if no value was set on certain fields
  foreach (array('state', 'minutes_estimate', 'accepted_estimate') as $field_name) {
    if (empty($comment->{"field_$field_name"}['und'][0]['value'])
      && !empty($previous_comment->{"field_$field_name"}['und'][0]['value'])) {
      $comment->{"field_$field_name"}['und'][0]['value']
        = $previous_comment->{"field_$field_name"}['und'][0]['value'];
    }
  }
}


function _bot_rym_get_tickets($as_user) {
//PUSH user
  global $user;
  $original_user = $user;
  $old_state = drupal_save_session();
  drupal_save_session(FALSE);

  $user = user_load($as_user);

//DO YOUR THING
  // basically taken from node.module default node frontpage
  $select = db_select('node', 'n')
//    ->fields('n', array('nid', 'sticky', 'created', 'title', 'og_group_ref'))
    ->fields('n', array('nid', 'sticky', 'created', 'title'))
    ->condition('status', 1)
    ->condition('type', 'ticket')
    ->orderBy('sticky', 'DESC')
    ->orderBy('created', 'DESC')
    ->extend('PagerDefault')
    ->limit(variable_get('default_nodes_main', 10))
    ->addTag('node_access');

  $nodes = array();
  foreach ($select->execute()->fetchAllAssoc('nid', PDO::FETCH_ASSOC) as $node) {
    $nodes[$node['nid']] = $node;
  }

//DONE YOUR THING
  $user = $original_user;
  drupal_save_session($old_state);

  return $nodes;
}

function _bot_rym_get_clients($as_user) {
//PUSH user
  global $user;
  $original_user = $user;
  $old_state = drupal_save_session();
  drupal_save_session(FALSE);

  $user = user_load($as_user);

//DO YOUR THING
  // basically taken from node.module default node frontpage
  $select = db_select('node', 'n')
//    ->join('field_data_og_group_ref', 'g', 'n.nid = g.entity_id')
//    ->fields('g', array(...))
    ->fields('n', array('nid', 'sticky', 'created', 'title'))
    ->condition('promote', 1)
    ->condition('status', 1)
    ->orderBy('sticky', 'DESC')
    ->orderBy('created', 'DESC')
    ->extend('PagerDefault')
    ->limit(variable_get('default_nodes_main', 10))
    ->addTag('node_access');

  $nids = $select->execute()->fetchAllKeyed(0, 3);
//DONE YOUR THING
  $user = $original_user;
  drupal_save_session($old_state);

  return $nids;
}

function _bot_rym_show_tickets($to, $data) {
  $account = bot_auth_authed($data);
  global $irc;
  if (empty($account->uid)) {
    bot_message($to, t('You must be authentified to do that. Type "!botname help auth" (no quotes) to see how.', array('!botname' => $irc->_nick)));
    return;
  }

  $nids = array_keys(_bot_rym_get_clients($account->uid));
  if (!count($nids)) {
    bot_message($to, 'No known clients or tickets.');
    return;
  }

  $s = array();
//  foreach (_bot_rym_get_tickets($account->uid) as $nid => $ticket) {
  foreach (_bot_rym_get_tickets($account->uid) as $ticket) {
//    echo "NID: $nid\n";
//    var_dump($ticket);
//    $s[$nid] = "{$ticket->title} ($nid) for {$ticket->og_group_ref}";
    $s[] = "{$ticket['title']} ({$ticket['nid']})";
  }
  bot_message($to, count($s) ? ('Tickets are: ' . join(', ', $s)) : 'No known tickets');
}

function _bot_rym_show_clients($to, $data) {
  $account = bot_auth_authed($data);
  global $irc;
  if (empty($account->uid)) {
    bot_message($to, t('You must be authentified to do that. Type "!botname help auth" (no quotes) to see how.', array('!botname' => $irc->_nick)));
    return;
  }

  $s = array();
  foreach (_bot_rym_get_clients($account->uid) as $nid => $client_name) {
    $s[] = "$client_name ($nid)";
  }
  bot_message($to, count($s) ? ('Clients are: ' . join(', ', $s)) : 'No known clients');
}

function _bot_rym_new_ticket($to, $data, $comment_message, $client_id = null) {
  $account = bot_auth_authed($data);

  //TODO
  if (empty($client_id)) {
    bot_message($to, t('You must pass a client_id (for now).'));
    return;
  }

  global $irc;
  if (empty($account->uid)) {
    bot_message($to, t('You must be authentified to do that. Type "!botname help auth" (no quotes) to see how.', array('!botname' => $irc->_nick)));
    return;
  }

  $clients = _bot_rym_get_clients($account->uid);
  if (empty($clients[$client_id])) {
    bot_message($to, t('You don\'t have access to that client.'));
    return;
  }

//PUSH user
  global $user;
  $original_user = $user;
  $old_state = drupal_save_session();
  drupal_save_session(FALSE);

  $user = user_load($account->uid);

//DO YOUR THING
  $node = new stdClass();
  $node->type = 'ticket';
  node_object_prepare($node);
  $node->uid = $account->uid;
  $node->title    = $comment_message;
  $node->language = LANGUAGE_NONE;
  $node->og_group_ref['und'][0]['target_id'] = $client_id;
  node_save($node);
  global $base_url;
  $nid = $node->nid;
  $x = "Created ticket #$nid at $base_url/node/$nid";
  bot_message($to, t("!nick: !message", array('!nick' => $data->nick, '!message' => $x)));
//DONE YOUR THING

  $user = $original_user;
  drupal_save_session($old_state);
}

function _bot_rym_punchin($to, $data, $comment_message, $ticket_id = null) {
  $account = bot_auth_authed($data);

  //TODO
  if (empty($ticket_id)) {
    bot_message($to, t('You must pass a ticket_id (for now).'));
    return;
  }

  global $irc;
  if (empty($account->uid)) {
    bot_message($to, t('You must be authentified to do that. Type "!botname help auth" (no quotes) to see how.', array('!botname' => $irc->_nick)));
    return;
  }

  $tickets = _bot_rym_get_tickets($account->uid);
  if (empty($tickets[$ticket_id])) {
    bot_message($to, t('You don\'t have access to that ticket.'));
    return;
  }


  // verifier qu'on peut vraiment puncher:
  // y'a un estimé en minutes et il est accepté

//PUSH user
  global $user;
  $original_user = $user;
  $old_state = drupal_save_session();
  drupal_save_session(FALSE);

  $user = user_load($account->uid);


  // taken from function rymd2pv_form_comment_node_ticket_form_alter...

//////////////////////////////////////////


  $query = db_select('comment', 'c');
  $query->innerJoin('node', 'n', 'n.nid = c.nid');
  $query->addTag('node_access');
  $comments_r = $query
    ->fields('c', array('cid'))
    ->condition('c.nid', $ticket_id)
    ->condition('c.status', COMMENT_PUBLISHED)
    ->condition('n.status', NODE_PUBLISHED)
    ->orderBy('c.created', 'DESC')
    // Additionally order by cid to ensure that comments with the same timestamp
    // are returned in the exact order posted.
    ->orderBy('c.cid', 'DESC')
    ->execute();

  $got_minutes_estimate = false;
  $got_accepted_estimate = false;
  $got_state = false;

  while ($cid = $comments_r->fetchColumn()) {
    $comment = comment_load($cid);
    if (!$got_state) {
      $got_state = $comment->field_state['und'][0]['value'];
    }

    if (!$got_accepted_estimate && ('accepted' === $comment->field_accepted_estimate['und'][0]['value'])) {
      $got_accepted_estimate = true;
    }

    if ($got_accepted_estimate && ($comment->field_minutes_estimate['und'][0]['value'] > 0)) {
      $got_minutes_estimate = intval($comment->field_minutes_estimate['und'][0]['value']);
      break;
    }
  }

  if (!($got_minutes_estimate && ('open/discuss' === $got_state))) {
    // can't punch here
    bot_message($to, 'Cannot punch in here.');
    $user = $original_user;
    drupal_save_session($old_state);

    return;
  }

  bot_message($to, t('OK to punch in, estimated time in minutes: !minutes.', array('!minutes' => $got_minutes_estimate)));

  $comment = new stdclass;
  $comment->nid = $ticket_id;
  $comment->uid = $account->uid;



  $comment->status = COMMENT_PUBLISHED; // We auto-publish this comment
  $comment->language = LANGUAGE_NONE; // The same as for a node
  $comment->subject = 'punch in';
  $comment->comment_body[$comment->language][0]['value'] = $comment_message;
  $comment->comment_body[$comment->language][0]['format'] = 'filtered_html';
  $comment->field_state[LANGUAGE_NONE][0]['value'] = 'active/working';
  // preparing a comment for a save
  comment_submit($comment);
  // saving a comment
  comment_save($comment);





//////////////////////////////////////////

//DONE YOUR THING

  $user = $original_user;
  drupal_save_session($old_state);


  bot_message($to, t('WORK IN PROGRESS - bon signe!.'));
  return;
}

/**
 * Handle ticket and client stuff.
 *
 * @param $data
 *   The regular $data object prepared by the IRC library.
 * @param $from_query
 *   Boolean; whether this was a queried request.
 */
function bot_rym_irc_msg_channel($data, $from_query = FALSE) {
  $to = $from_query ? $data->nick : $data->channel;
  $addressed = bot_name_regexp();

  if (preg_match("/^($addressed)show clients$/i", $data->message)
   || preg_match("/^show clients$/i", $data->message)) {
     return _bot_rym_show_clients($to, $data);
  }

  if (preg_match("/^($addressed)show tickets$/i", $data->message)
   || preg_match("/^show tickets$/i", $data->message)) {
     return _bot_rym_show_tickets($to, $data);
  }


  if (preg_match("/^($addressed)ticket for ([\d]+): (.*)$/i", $data->message, $matches)
   || preg_match("/^ticket for ([\d]+): (.*)$/i", $data->message, $matches)) {
     return empty($matches[3])
       ? _bot_rym_new_ticket($to, $data, $matches[2], $matches[1])
       : _bot_rym_new_ticket($to, $data, $matches[3], $matches[2]);
  }

//TODO (see function _bot_rym_new_ticket())
  if (preg_match("/^($addressed)ticket: (.*)$/i", $data->message, $matches)
   || preg_match("/^ticket: (.*)$/i", $data->message, $matches)) {
     return empty($matches[2])
       ? _bot_rym_new_ticket($to, $data, $matches[1])
       : _bot_rym_new_ticket($to, $data, $matches[2]);
  }




  if (preg_match("/^($addressed)pi ([\d]+): (.*)$/i", $data->message, $matches)
   || preg_match("/^pi ([\d]+): (.*)$/i", $data->message, $matches)) {
     return empty($matches[3])
       ? _bot_rym_punchin($to, $data, $matches[2], $matches[1])
       : _bot_rym_punchin($to, $data, $matches[3], $matches[2]);
  }



  if (preg_match("/^($addressed)po ([\d]+): (.*)$/i", $data->message, $matches)
   || preg_match("/^po ([\d]+): (.*)$/i", $data->message, $matches)) {
     return empty($matches[3])
       ? _bot_rym_punchout($to, $data, $matches[2], $matches[1])
       : _bot_rym_punchout($to, $data, $matches[3], $matches[2]);
  }


}

/**
 * All responses are available via a query.
 */
function bot_rym_irc_msg_query($data) {
  bot_rym_irc_msg_channel($data, TRUE);
}

/*
// TODO
function _bot_rym_push_user($u = null) {
}

function _bot_rym_pop_user() {
  return _bot_rym_push_user();
}
*/



////////////////////////



function _bot_rym_punchout($to, $data, $comment_message, $ticket_id = null) {
  $account = bot_auth_authed($data);

  //TODO
  if (empty($ticket_id)) {
    bot_message($to, t('You must pass a ticket_id (for now).'));
    return;
  }

  global $irc;
  if (empty($account->uid)) {
    bot_message($to, t('You must be authentified to do that. Type "!botname help auth" (no quotes) to see how.', array('!botname' => $irc->_nick)));
    return;
  }

  $tickets = _bot_rym_get_tickets($account->uid);
  if (empty($tickets[$ticket_id])) {
    bot_message($to, t('You don\'t have access to that ticket.'));
    return;
  }


  // verifier qu'on peut vraiment puncher:
  // y'a un estimé en minutes et il est accepté

//PUSH user
  global $user;
  $original_user = $user;
  $old_state = drupal_save_session();
  drupal_save_session(FALSE);

  $user = user_load($account->uid);


  // taken from function rymd2pv_form_comment_node_ticket_form_alter...

//////////////////////////////////////////


  $query = db_select('comment', 'c');
  $query->innerJoin('node', 'n', 'n.nid = c.nid');
  $query->addTag('node_access');
  $comments_r = $query
    ->fields('c', array('cid'))
    ->condition('c.nid', $ticket_id)
    ->condition('c.status', COMMENT_PUBLISHED)
    ->condition('n.status', NODE_PUBLISHED)
    ->orderBy('c.created', 'DESC')
    // Additionally order by cid to ensure that comments with the same timestamp
    // are returned in the exact order posted.
    ->orderBy('c.cid', 'DESC')
    ->execute();

  $got_minutes_estimate = false;
  $got_accepted_estimate = false;
  $got_state = false;

  while ($cid = $comments_r->fetchColumn()) {
    $comment = comment_load($cid);
    if (!$got_state) {
      $got_state = $comment->field_state['und'][0]['value'];
    }

    if (!$got_accepted_estimate && ('accepted' === $comment->field_accepted_estimate['und'][0]['value'])) {
      $got_accepted_estimate = true;
    }

    if ($got_accepted_estimate && ($comment->field_minutes_estimate['und'][0]['value'] > 0)) {
      $got_minutes_estimate = intval($comment->field_minutes_estimate['und'][0]['value']);
      break;
    }
  }

  if (!($got_minutes_estimate && ('active/working' === $got_state))) {
    // can't punch here
    bot_message($to, 'Cannot punch out here.');
    $user = $original_user;
    drupal_save_session($old_state);

    return;
  }

  bot_message($to, t('OK to punch out, estimated time in minutes: !minutes.', array('!minutes' => $got_minutes_estimate)));

  $comment = new stdclass;
  $comment->nid = $ticket_id;
  $comment->uid = $account->uid;



  $comment->status = COMMENT_PUBLISHED; // We auto-publish this comment
  $comment->language = LANGUAGE_NONE; // The same as for a node
  $comment->subject = 'punch out';
  $comment->comment_body[$comment->language][0]['value'] = $comment_message;
  $comment->comment_body[$comment->language][0]['format'] = 'filtered_html';
  $comment->field_state[LANGUAGE_NONE][0]['value'] = 'open/discuss';
  // preparing a comment for a save
  comment_submit($comment);
  // saving a comment
  comment_save($comment);





//////////////////////////////////////////

//DONE YOUR THING

  $user = $original_user;
  drupal_save_session($old_state);


  bot_message($to, t('WORK IN PROGRESS - bon signe!.'));
  return;
}
